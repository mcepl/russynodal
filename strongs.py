# -*- coding: utf8 -*-

# todo, for next document use for synonims table like
# ['сын', 'сынов', 'сыновей'] instead of dictionary and find ignore case

import os.path, os, sys, re, difflib, copy, urllib, urllib2

#            OSIS      Testament  BibleQuote         USFM
bookDb = [ ['Gen',    'ot',      'Genesis',         'GEN'],
            ['Exod',   'ot',      'Exodus',          'EXO'],
            ['Lev',    'ot',      'Leviticus',       'LEV'],
            ['Num',    'ot',      'Numbers',         'NUM'],
            ['Deut',   'ot',      'Deuteronomy',     'DEU'],
            ['Josh',   'ot',      'Joshua',          'JOS'],
            ['Judg',   'ot',      'Judges',          'JDG'],
            ['Ruth',   'ot',      'Ruth',            'RUT'],
            ['1Sam',   'ot',      '1Samuel',         '1SA'],
            ['2Sam',   'ot',      '2Samuel',         '2SA'],
            ['1Kgs',   'ot',      '1Kings',          '1KI'],
            ['2Kgs',   'ot',      '2Kings',          '2KI'],
            ['1Chr',   'ot',      '1Chron',          '1CH'],
            ['2Chr',   'ot',      '2Chron',          '2CH'],
            ['Ezra',   'ot',      'Ezra',            'EZR'],
            ['Neh',    'ot',      'Nehemiah',        'NEH'],
            ['1Esd',   'otnc',    '2 Ездры'          ],
            ['Tob',    'otnc',    'Товит'            ],
            ['Jdt',    'otnc',    'Иудифь'           ],
            ['Esth',   'ot',      'Esther',          'EST'],
            ['Job',    'ot',      'Job',             'JOB'],
            ['Ps',     'ot',      'Psalm',           'PSA'],
            ['Prov',   'ot',      'Proverbs',        'PRO'],
            ['Eccl',   'ot',      'Ecclesia',        'ECC'],
            ['Song',   'ot',      'Song',            'SNG'],
            ['Wis',    'otnc',    'Премудрость'      ],
            ['Sir',    'otnc',    'Сирах'            ],
            ['Isa',    'ot',      'Isaiah',          'ISA'],
            ['Jer',    'ot',      'Jeremiah',        'JER'],
            ['Lam',    'ot',      'Lament',          'LAM'],
            ['EpJer',  'otnc',    'Послание Иеремии' ],
            ['Bar',    'otnc',    'Варух'            ],
            ['Ezek',   'ot',      'Ezekiel',         'EZK'],
            ['Dan',    'ot',      'Daniel',          'DAN'],
            ['Hos',    'ot',      'Hosea',           'HOS'],
            ['Joel',   'ot',      'Joel',            'JOL'],
            ['Amos',   'ot',      'Amos',            'AMO'],
            ['Obad',   'ot',      'Obadiah',         'OBA'],
            ['Jonah',  'ot',      'Jonah',           'JON'],
            ['Mic',    'ot',      'Micah',           'MIC'],
            ['Nah',    'ot',      'Nahum',           'NAM'],
            ['Hab',    'ot',      'Habakkuk',        'HAB'],
            ['Zeph',   'ot',      'Zephaniah',       'ZEP'],
            ['Hag',    'ot',      'Haggai',          'HAG'],
            ['Zech',   'ot',      'Zechariah',       'ZEC'],
            ['Mal',    'ot',      'Malachi',         'MAL'],
            ['1Macc',  'otnc',    '1 Макавейская'    ],
            ['2Macc',  'otnc',    '2 Макавейская'    ],
            ['3Macc',  'otnc',    '3 Макавейская'    ],
            ['2Esd',   'otnc',    '3 Ездры'          ],
            ['Matt',   'nt',      'Matthew',         'MAT'],
            ['Mark',   'nt',      'Mark',            'MRK'],
            ['Luke',   'nt',      'Luke',            'LUK'],
            ['John',   'nt',      'John',            'JHN'],
            ['Acts',   'nt',      'Acts',            'ACT'],
            ['Jas',    'nt',      'James',           'JAS'],
            ['1Pet',   'nt',      '1Peter',          '1PE'],
            ['2Pet',   'nt',      '2Peter',          '2PE'],
            ['1John',  'nt',      '1John',           '1JN'],
            ['2John',  'nt',      '2John',           '2JN'],
            ['3John',  'nt',      '3John',           '3JN'],
            ['Jude',   'nt',      'Jude',            'JUD'],
            ['Rom',    'nt',      'Romans',          'ROM'],
            ['1Cor',   'nt',      '1Corinthians',    '1CO'],
            ['2Cor',   'nt',      '2Corinthians',    '2CO'],
            ['Gal',    'nt',      'Galatians',       'GAL'],
            ['Eph',    'nt',      'Ephesian',        'EPH'],
            ['Phil',   'nt',      'Philippians',     'PHP'],
            ['Col',    'nt',      'Colossians',      'COL'],
            ['1Thess', 'nt',      '1Thessalonians',  '1TH'],
            ['2Thess', 'nt',      '2Thessalonians',  '2TH'],
            ['1Tim',   'nt',      '1Timothy',        '1TI'],
            ['2Tim',   'nt',      '2Timothy',        '2TI'],
            ['Titus',  'nt',      'Titus',           'TIT'],
            ['Phlm',   'nt',      'Philemon',        'PHM'],
            ['Heb',    'nt',      'Hebrews',         'HEB'],
            ['Rev',    'nt',      'Revelation',      'REV']]

# find entry in book database and return geven field
def bookData(name, field):
    name = name.lower()

    for b in bookDb:
        if b[0].lower() == name:
            return b[field]

    # if not found do second iteration
    for b in bookDb:
        for bb in b[2:len(b)]:
            if bb.lower() == name:
                return b[field]

    assert(false)

def addProcessor(proc):
    global processors
    processors.append(proc)

def clearProcessors():
    global processors
    processors = []

def processFile(inputFile, outputFile):
    f = open(inputFile)
    o = open(outputFile, 'w')
    
    for l in f:
        for p in processors:
            r = p(l)
            if r == True:
                break
            elif r == False:
                ()
            else:
                l = r
        o.write(l)

    f.close()
    o.close()

def processDir(dir):
    global openedFile
    
    for f in os.listdir(dir):
        openedFile = os.path.abspath(dir + '/' + f)
        processFile(openedFile)

def parseUsfmPosition(l):
    global osisBook, osisChapter, osisVerse, openedFile

    if not openedFile.endswith('.usfm'):
        return False
            
    l = l.rstrip('\n').strip(' ')
    
    if l.startswith('\\id'):
        osisBook = bookData(l.strip('\\id').strip(' '), 0)
        assert(len(osisBook) > 0)
    elif l.startswith('\\c'):
        osisChapter = int(re.findall('(\d+)', l)[0])
    elif l.startswith('\\v'):
        osisVerse = int(re.findall('(\d+)', l)[0])

    return False

def saveDb(db, f):
    ff = open(f, 'w')
    if len(db) > 0:
        ff.write('[')
        for i in range(0, len(db)):
            if i > 0:
                ff.write(',\n ')
            ff.write('[\'' + db[i][0] + '\'')
            for ii in db[i][1:len(db[i])]:
                ff.write(', [\'' + ii[0])
                for iii in ii[1:len(ii)]:
                    ff.write('\', \'' + iii)
                ff.write('\']')
            ff.write(']')
        ff.write(']\n')
    ff.close()

def loadValue(f):
    ff = open(f)
    r = eval(ff.read())
    ff.close()
    return r

def generate(dir, db):
    global sgDb
    sgDb = []
    
    def parseUsfmVerse(l):
        global osisBook, osisChapter, osisVerse, openedFile, sgDb
                
        if not openedFile.endswith('.usfm'):
            return False
    
        l = l.rstrip('\n')

        if l.startswith('\\v '):
            l = l[l.find(' ', 3):len(l)].lstrip(' ')

            data = [['', '']]
            number = 0

            # collect strongs
            i = 0
            while i < len(l):
                c = l[i]
                i += 1
                
                if c == '<':
                    # for tags read whole tag
                    e = l.find('>', i)
                    d = ''
                    number = 1;
                    for ii in range(i, e):
                        if l[ii].isdigit():
                            d += l[ii]
                    if len(data[len(data) - 1][1]) > 0:
                        d = ' ' + d
                    data[len(data) - 1][1] += d
                    i = e
                    continue
                elif c == '>':
                    continue
                elif c.isdigit() and number == 0:
                    number = 1
                elif not c.isdigit() and number == 1 and c != ' ':
                    number = 0
                    data.append(['', ''])

                if c.isalpha() and number == 0:
                    s = data[len(data) - 1][0]
                    if len(s) > 0 and s[len(s) - 1] == ' ':
                        data[len(data) - 1][0] = ''
                     
                data[len(data) - 1][number] += c

            # remove empty strongs, and modify data
            i = 0
            while i < len(data):
                if len(data[i][1]) == 0 or data[i][1].isspace() or \
                   len(data[i][0]) == 0 or data[i][0].isspace():
                    del data[i]
                    i -= 1
                else:
                    d = [data[i][0], data[i][1]]

                    # strip out nunecessary characters                    
                    d[0] = d[0].rstrip(' ')
                    d[1] = d[1].rstrip(' ').lstrip(' ')

                    if d[0].rfind(' ') != -1:
                        d[0] = d[0][d[0].rfind(' '):len(d[0])]

                    while not d[0][0].isalpha():
                        d[0] = d[0][1:len(d[0])]
                        if len(d[0]) == 0:
                            print 'Something wrong with', \
                                data[i][0].decode('utf8'), data[i][1]
                            break;

                    if d[0].find(' ') != -1:
                        print 'Wrong character', '[' + d[0].decode('utf8') + \
                              ']', data[i][0].decode('utf8'), data[i][1]

                    # convert strongs
                    ns = d[1].split(' ')
                    d[1] = ''

                    for n in ns:
                        n = str(int(n))
                        if bookData(osisBook, 1).startswith('ot'):
                            n = 'H' + n
                        else:
                            n = 'G' + n
                            
                        if len(d[1]) == 0:
                            d[1] += n
                        else:
                            d[1] += ' ' + n

                    # swap
                    data[i][0] = d[1]
                    data[i][1] = d[0]
                i += 1

            if len(data) == 0:
                return False

            sgDb.append([osisBook + '.' + str(osisChapter) + '.' + \
                      str(osisVerse)] + data)

    clearProcessors()
    addProcessor(parseUsfmPosition)
    addProcessor(parseUsfmVerse)

    processDir(dir)

    saveDb(sgDb, db)

def assign(dbFile, inFile, outFile, synonims=None):
    global sgDb, skip, wordDict, osisId

    sgDb = loadValue(dbFile)
    wordDict = loadValue(synonims)
    skip = False
    osisId = ''
    
    # for future
    def translate(text, source_lang, dest_lang):
        url = "http://translate.google.com/translate_a/t?%s"
        list_of_params = {'client' : 't', 'hl' : 'en', 'multires' : '1', \
            'text' : text, 'sl' : source_lang, 'tl' : dest_lang}

        request = urllib2.Request(url % urllib.urlencode(list_of_params), 
            headers={ 'User-Agent': 'Mozilla/5.0', 'Accept-Charset': 'utf-8' })
        res = urllib2.urlopen(request).read()

        fixed_json = re.sub(r',{2,}', ',', res).replace(',]', ']')  
        data = json.loads(fixed_json)
     
        #simple translation
        print "%s / %s / %s" % (data[0][0][0], data[0][0][1], 
            data[0][0][2] or data[0][0][3])
     
        #abbreviation
        if not isinstance(data[1], basestring):
            print data[1][0][0]
            print_params(data[1][0][1])
       
        #interjection  
        try:
            if not isinstance(data[1][1], basestring):
                print data[1][1][0]
                print_params(data[1][1][1])
        except Exception:
            print "no interjection"

    # define guess strategy
    class strategy:
        def __init__(self, ratio):
            self.fzRt = ratio # accept ratio for fuzzy find

        def process(self, text, strongs):
            self.tx = text
            self.sgDt = strongs
            self.otPt = ''
            
            e = 1
            p = 0
            while e < len(self.sgDt):
                bn = 0
                ed = 0
                s = self.sgDt[e][0]
                d = self.sgDt[e][1:len(self.sgDt[e])]

                # translate from dictionary
                for i in d:
                    if i == '':
                        continue
                    for ii in range(0, len(wordDict)):
                        try:
                            if i in wordDict[ii] and \
                                d.count(wordDict[ii][i]) == 0:
                                d.append(wordDict[ii][i])
                        except:
                            print 'Wrong dict', d, text
                    
                # find using regex
                for i in d:
                    if i == '':
                        continue
                    ex = u'(\W|^)(' + i.decode('utf8') + u')(\W|$)'
                    try:
                        m = re.compile(ex, re.U | re.I).search(self.tx, p)
                        if m != None and  m.start(2) != m.end(2) and \
                           (bn == ed or bn > m.start(2)):
                            bn = m.start(2)
                            ed = m.end(2)
                    except re.error, v:
                        self.otPt += 'RegEx error: ' + v + ' ' + ex + '\n'
                        
                # fuzzy find
                if self.fzRt < 1.0 and bn == ed:
                    fzEx = re.compile('(\w+)', re.U | re.I)
                    fzP = p
                    fzRt = 0.0
                    fzWd = ''
                    fzFf = 0.0
                    while True:
                        m = fzEx.search(self.tx, fzP)
                        
                        if m == None:
                            break
                        else:
                            mS = m.start(1)
                            mE = m.end(1)

                            # accents usualy break the word
                            if mE < len(self.tx) and self.tx[mE] == u'́':
                                m = fzEx.search(self.tx, mE + 1)
                                if m != None:
                                    mE = m.end(1)

                            fzP = mE + 1
                            tw = self.tx[mS:mE]

                            for i in d:
                                if i == '':
                                    continue
                                dw = i.decode('utf8').replace(u'ё', u'е').replace(u'Ё', u'Е')
                                rt = difflib.SequenceMatcher(None, dw, tw).ratio()
                                if rt > fzRt + fzFf and rt > self.fzRt:
                                    fzRt = rt
                                    bn = mS
                                    ed = mE
                                    fzWd = dw
                                        
                        fzFf += 0.005

                    # check fuzzy matching
                    if bn < ed and fzRt < 0.8:
                        self.otPt += 'Fuzzy match: ' + osisId + ' ' + \
                            fzWd + ' ' + self.tx[bn:ed] + ' ' + \
                            str(fzRt)[0:5] + '\n'

                # empty word
                empty = False
                if d.count('') == 1:
                    #if bn < ed:
                    #    self.otPt += 'Empty insertion, but word found: ' + \
                    #                  osisId + '\n'
                    bn = ed = p
                    empty = True
                    #self.otPt += 'Empty insert: ' + osisId + ' ' + s + '\n'

                # insert to line
                if bn < ed or empty:
                    # separate lemmas and morphs
                    ls = []
                    ms = []
                    for i in s.split(' '):
                        if len(i) < 2 or i[1:len(i)].isdigit() == False:
                            print 'Wrong strongs:', osisId,  s
                            continue
                        if (i[0] == 'H' and int(i[1:len(i)]) > 8674) or \
                            (i[0] == 'G' and int(i[1:len(i)]) > 5624):
                            ms.append(i)
                        else:
                            ls.append(i)

                    ww = '<w'
                    if len(ls) > 0:
                        ww += ' lemma="strong:' + ls[0]
                        for i in ls[1:len(ls)]:
                            ww += ' strong:' + i
                        ww += '"'
                    if len(ms) > 0:
                        ww += ' morph="strongMorph:T' + ms[0]
                        for i in ms[1:len(ms)]:
                            ww += ' strongMorph:T' + i
                        ww += '"'

                    wo = '</w>'

                    if empty:
                        ww += '/>'
                        wo = ''
                    else:
                        ww += '>'

                    # todo handle/test H0 and G0 strongs

                    if len(wo) > 0:
                        self.tx = self.tx[0:ed] + wo + \
                                  self.tx[ed:len(self.tx)]
                    self.tx = self.tx[0:bn] + ww + self.tx[bn:len(self.tx)]

                    p = ed + len(wo) + len(ww)
                    del self.sgDt[e]
                    
                    e -= 1
                    
                e += 1

    strategies = [strategy(0.62), strategy(1.0), strategy(0.8)]

    def parseOsis(l):
        global sgDb, sgId, osisId, wordDict, skip

        l = l.rstrip('\n').strip(' ').decode('utf8')
        doOutput = True
        sgId = len(sgDb) + 1
        doProcess = False
            
        if l.startswith('\t\t<verse '):
            cId = re.findall('osisID="(\w+.\d+.\d+)"', l)

            if len(cId) > 0:
                osisId = cId[0]

                # setup scope
                if osisId == 'Gen.1.1':
                    skip = False
                if osisId == 'Rev.111.111':
                    skip = True
            doProcess = True
            
        elif l.startswith('\t<chapter '):
            osisId = re.findall('osisID="(\w+.\d+)"', l)[0]
            
        elif l.startswith('\t\t<title type="psalm" canonical="true">'):
            osisId = osisId + '.0'
            doProcess = True

        # process text line
        if len(osisId) > 0 and doProcess == True and not skip:
            # find strongs entry
            sgId = 0

            while sgId < len(sgDb):
                if sgDb[sgId][0] == osisId:
                    break
                sgId += 1

            if sgId == len(sgDb):
                print 'No strongs for:', osisId
            else:
                # try strategies
                best = 0
                left = len(sgDb[sgId])
                for i, v in enumerate(strategies):
                        v.process(l, copy.deepcopy(sgDb[sgId]))

                        if len(v.sgDt) < left:
                                left = len(v.sgDt)
                                best = i

                        if left == 1:
                                break

                # apply best strategy
                if len(strategies[best].otPt) > 0 and doOutput:
                        print strategies[best].otPt.rstrip('\n')

                sgDb[sgId] = strategies[best].sgDt
                l = strategies[best].tx

        # finaly remove strongs entry to check system correct work
        if not skip and sgId < len(sgDb):
            if len(sgDb[sgId]) > 1 and doOutput:
                print 'Not all strongs assigned:', sgDb[sgId][0]
                for i in sgDb[sgId][1:len(sgDb[sgId])]:
                    print '\t', i[0], '', i[1].decode('utf8')
            del sgDb[sgId]
            sgId = len(sgDb)
                
        if not skip:
            return (l.encode('utf8') + '\n')
        return ''

    def fixOsis4Synodal(l):
        l = l.decode('utf8')
        l = l.replace(u'<w lemma="strong:G2076" morph="strongMorph:TG5748">–</w> это',
                      u'– <w lemma="strong:G2076" morph="strongMorph:TG5748">это</w>')
        l = l.replace(u'<w lemma="strong:G1526" morph="strongMorph:TG5748">–</w> это',
                      u'– <w lemma="strong:G1526" morph="strongMorph:TG5748">это</w>')
        l = l.replace(u'<w lemma="strong:G2076" morph="strongMorph:TG5748">–</w> есть',
                      u'– <w lemma="strong:G2076" morph="strongMorph:TG5748">есть</w>')
        if l.startswith('<verse sID="Ps.') and l.find('.0"') >= 0:
            l = '<title type="psalm" canonical="true">\n'
        if l.startswith('<verse eID="Ps.') and l.find('.0"') >= 0:
            l = '</title>\n'
        return l.encode('utf8')

    clearProcessors()
    addProcessor(parseOsis)
    #addProcessor(fixOsis4Synodal)
    processFile(inFile, outFile)

    for i in sgDb:
        print 'Not used strongs for:', i[0]

#usage
#generate('../IBT', 'db_ibt')
#todo: translate('db', 'language')
assign('db_ibt', 'RusSynodal.osis.xml', 'RusSynodalStrongs.osis.xml', 'synonims')
